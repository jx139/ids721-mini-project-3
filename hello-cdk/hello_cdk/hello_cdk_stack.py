# Use AWS CDK to create an S3 bucket with versioning enabled and AES-256 encryption
import aws_cdk as cdk

from aws_cdk import (
  Stack,
  aws_s3 as s3
)
from constructs import Construct

class S3Stack(Stack):

  def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
    super().__init__(scope, construct_id, **kwargs)

    # Create an S3 bucket with versioning and AES-256 encryption
    s3.Bucket(self, "XXXXXXXX",
      versioned=True,
      encryption=s3.BucketEncryption.KMS_MANAGED
    )
