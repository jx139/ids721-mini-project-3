# AWS CDK S3 Bucket Creation with Versioning and Encryption
![pipeline status](https://gitlab.oit.duke.edu/jx139/ids721-mini-project-3/badges/main/pipeline.svg)

This repository contains code for creating an Amazon S3 bucket with versioning enabled and encryption using the AWS Cloud Development Kit (AWS CDK). AWS CodeWhisperer was utilized to help generate some of the CDK code for this project.

## Prerequisites

Before you begin, ensure you have the following:

- An AWS account
- AWS CLI installed and configured with your credentials
- Node.js and npm installed
- AWS CDK Toolkit installed (`npm install -g aws-cdk`)
- Python 3.6 or later
- Your preferred IDE with AWS CodeWhisperer set up for generating code

## Setup and Deployment

To deploy the S3 bucket with the necessary properties, follow these steps:

1. Clone this repository to your local machine.
2. Navigate to the repository directory in your terminal.
3. Run `python -m pip install -r requirements.txt` to install the required dependencies.
4. Synthesize the CloudFormation template from the CDK code (optional) with `cdk synth`.
5. Deploy the stack to your AWS account with `cdk deploy`.

Upon successful deployment, an S3 bucket with versioning and encryption will be created.

## S3 Bucket Features

- **Versioning**: Versioning is enabled on this S3 bucket, allowing you to preserve, retrieve, and restore every version of every object stored in your bucket.
- **Encryption**: The bucket uses s3.BucketEncryption.KMS_MANAGED for encryption-at-rest, ensuring that your data is secure.
![cdk](./img.png)

## Using AWS CodeWhisperer

AWS CodeWhisperer was used to generate the CDK code snippets for creating the S3 bucket with the specified properties. CodeWhisperer is an AI-powered service that provides code recommendations based on natural language comments or code context within your IDE.

To use AWS CodeWhisperer in your project:

1. Write a descriptive comment in your IDE, such as `# create an S3 bucket with versioning and AES-256 encryption`.
2. Press `option+c` (or the respective trigger in your IDE) to invoke CodeWhisperer.
3. Review the suggestions provided by CodeWhisperer and choose the one that fits your needs.
4. Insert the suggestion into your code and adjust as necessary to fit into your application's context.

## Repository Structure

- `cdk.json`: The CDK configuration file that defines how the CDK is executed.
- `app.py`: The main entry point for the CDK application.
- `requirements.txt`: The file listing the Python dependencies for this project.
- `lib/`: The directory where the main stack of the application is defined.
